package org.admin.config;

import com.rabbitmq.tools.json.JSONUtil;
import org.admin.sender.Sender;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.retry.interceptor.StatefulRetryOperationsInterceptor;

import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;

@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue schedulerQueue() {
        return new Queue("adminSendQueue1");
    }



    public static class SenderConfig {

        @Autowired
        private Queue queue;


        @Bean
        public Sender sender() {
            System.out.println(queue.getName());
            System.out.println(queue.getName());
            System.out.println(queue.getName());
            System.out.println(queue.getName());
            return new Sender();
        }

    }

    @Autowired
    private Sender sendere;
    @Bean
    public StatefulRetryOperationsInterceptor interceptor() {

        System.out.println(sendere);

        return RetryInterceptorBuilder.stateful()
                .maxAttempts(5)
                .build();
    }


}
