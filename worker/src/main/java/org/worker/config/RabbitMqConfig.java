package org.worker.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.worker.receiver.Receiver;

@Configuration
public class RabbitMqConfig {


    @Bean
    Receiver receiver(){
        return new Receiver();
    }

}
